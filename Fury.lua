local function fury_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- Power
    Helper.player.rage = UnitPower("player")
    
    -- Enrage
    local name, _, exp = Helper:UnitBuff("player", "Enrager")
    if name == nil then
        Helper.player.enrager = false
    else
        Helper.player.enrager = true
    end
    
end

local function fury_target_info()
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
end

local function fury_select_spell()
    local health = Helper.player.health
    local rage = Helper.player.rage
    local enrager = Helper.player.enrager
    
    -- print("     -----")
    -- print("health    : " .. tostring(health))
    -- print("lunaire   : " .. tostring(lunaire))
    -- print("enrager      : " .. tostring(enrager))
    -- print("givre   : " .. tostring(givre))
    -- print("lunaire   : " .. tostring(renf_lunaire))
    -- print("oneth   : " .. tostring(oneth))
    -- print("meteores: " .. tostring(meteores))
    -- print("casting   : " .. tostring(casting))
    -- print("     -----")
    
    -- -------------------------
    -- Heal phase
    -- -------------------------

    if health < 0.30 then
        local spell = Helper.spell.regeneration
        if Helper:usable_ranged_spell(spell) == true then
            return spell
        end
    end
    if health < 0.90 then
        local spell = Helper.spell.ivresse
        if Helper:usable_ranged_spell(spell) == true then
            return spell
        end
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------

    local spell = Helper.spell.charge
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
        
    local spell = Helper.spell.saccager
    if Helper:usable_ranged_spell(spell) == true and rage >= 90 and not enrager then
        return spell
    end
    
    local spell = Helper.spell.temerite
    if Helper:usable_spell(spell) == true then
        return spell
    end
        
    local spell = Helper.spell.execution
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end

    local spell = Helper.spell.dechaine
    if Helper:usable_ranged_spell(spell) == true and enrager then
        return spell
    end
        
    local spell = Helper.spell.sanguinaire
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
        
    local spell = Helper.spell.dragon
    if Helper:usable_spell(spell) == true and enrager then
        return spell
    end
        
    local spell = Helper.spell.dechaine
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
        
    local spell = Helper.spell.tourbillon
    if Helper:usable_spell(spell) == true then
        return spell
    end        
        
    return nil
end

local function fury_select_cut()
    local cut = Helper.spell.volee
    local casting = Helper.target.casting
    
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

function Helper:fury_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.rage = 1.0
    Helper.player.enrager = false
    
    -- target info
    Helper.target = {}
    
    -- spells
    Helper.spell = {}
    Helper.spell.saccager = "Saccager"
    Helper.spell.temerite = "Témérité"
    Helper.spell.execution = "Exécution"
    Helper.spell.dechaine = "Coup déchaîné"
    Helper.spell.sanguinaire = "Sanguinaire"
    Helper.spell.dragon = "Rugissement de dragon"
    Helper.spell.tourbillon = "Tourbillon"
    Helper.spell.charge = "Charge"
    Helper.spell.ivresse = "Ivresse de la victoire"

    Helper.spell.regeneration = "Régénération enragée"

    Helper.spell.volee = "Volée de coups"
    
    -- functions
    Helper.player_info = fury_player_info
    Helper.target_info = fury_target_info
    Helper.select_spell = fury_select_spell
    Helper.select_cut = fury_select_cut
end
