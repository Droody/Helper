local function balance_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- Power
    Helper.player.power = UnitPower("player")
    
    -- Eclat lunaire
    local name, count, exp = Helper:UnitBuff("player", "Renforcement lunaire")
    if name == nil then
        Helper.player.lunaire = 0
    else
        Helper.player.lunaire = count
    end
    
    -- Eclat solaire
    local name, count, exp = Helper:UnitBuff("player", "Renforcement solaire")
    if name == nil then
        Helper.player.solaire = 0
    else
        Helper.player.solaire = count
    end
    
    -- Meteores
    local name, _, _ = Helper:UnitBuff("player", "Météores")
    if name == nil then
        Helper.player.meteores = false
    else
        Helper.player.meteores = true
    end
    
    --[[-- Oneth
local name, _, _ = Helper:UnitBuff("player", "Suffisance d’Oneth")
if name == nil then
Helper.player.oneth = false
else
Helper.player.oneth = true
end
--]]
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("player")
    if name then
        Helper.player.casting = name
    else
        Helper.player.casting = nil
    end
end

local function balance_target_info()
    
    -- Eclat lunaire
    local name, _, exp = Helper:UnitDebuff("target", "Éclat lunaire")
    if name == nil or exp < 2.0 then
        Helper.target.lunaire = false
    else
        Helper.target.lunaire = true
    end
    
    -- Solaire
    local name, _, exp = Helper:UnitDebuff("target", "Éclat solaire")
    if name == nil or exp < 2.0 then
        Helper.target.solaire = false
    else
        Helper.target.solaire = true
    end
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
end

local function balance_select_spell()
    local health = Helper.player.health
    local power = Helper.player.power
    local meteores = Helper.player.meteores
    local oneth = Helper.player.oneth
    local lunaire = Helper.target.lunaire
    local solaire = Helper.target.solaire
    local renf_lunaire = Helper.player.lunaire
    local renf_solaire = Helper.player.solaire
    local casting = Helper.player.casting
    
    -- print("     -----")
    -- print("health    : " .. tostring(health))
    -- print("lunaire   : " .. tostring(lunaire))
    -- print("solaire   : " .. tostring(solaire))
    -- print("lunaire   : " .. tostring(renf_lunaire))
    -- print("oneth   : " .. tostring(oneth))
    -- print("meteores: " .. tostring(meteores))
    -- print("casting   : " .. tostring(casting))
    -- print("     -----")
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.25 then
        -- Ecorse
        local spell = Helper.spell.ecorse
        if Helper:usable_spell(spell) == true and spell ~= casting then
            return spell
        end
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------
    -- 1/ Not in Compobat -> courroux
    -- local spell = Helper.spell.courroux
    -- if Helper:usable_ranged_spell(spell) == true and not UnitAffectingCombat("player") then
    -- return spell
    -- end
    
    -- Eruption si power >= 100
    local spell = Helper.spell.eruption
    if Helper:usable_ranged_spell(spell) == true and power >= 100 and spell ~= casting then
        return spell
    end
    
    -- Solaire
    local spell = Helper.spell.solaire
    if Helper:usable_ranged_spell(spell) == true and not solaire and spell ~= casting then
        return spell
    end
    -- Lunaire
    local spell = Helper.spell.lunaire
    if Helper:usable_ranged_spell(spell) == true and not lunaire and spell ~= casting then
        return spell
    end
    
    -- Frappe si 3 charges
    local spell = Helper.spell.frappe
    if Helper:usable_ranged_spell(spell) == true and renf_lunaire == 3 and spell ~= casting then
        return spell
    end
    -- Courroux si 3 charges
    local spell = Helper.spell.courroux
    if Helper:usable_ranged_spell(spell) == true and renf_solaire == 3 and spell ~= casting then
        return spell
    end
    
    -- Eruption si power >= 60
    local spell = Helper.spell.eruption
    if Helper:usable_ranged_spell(spell) == true and power >= 60 and spell ~= casting then
        return spell
    end
    
    -- Guerrier
    local spell = Helper.spell.guerrier
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    -- -- Eruption
    -- local spell = Helper.spell.eruption
    -- if Helper:usable_ranged_spell(spell) == true and spell ~= casting then
    -- return spell
    -- end
    
    -- Frappe si renf >
    local spell = Helper.spell.frappe
    if Helper:usable_ranged_spell(spell) == true and renf_lunaire > renf_solaire and spell ~= casting then
        return spell
    end
    
    -- Courroux
    local spell = Helper.spell.courroux
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    return nil
end

local function balance_select_cut()
    local cut = Helper.spell.rayon
    local casting = Helper.target.casting
    
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

function Helper:balance_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.power = 1.0
    Helper.player.lunaire = 0
    Helper.player.solaire = 0
    Helper.player.meteores = false
    Helper.player.oneth = false
    Helper.player.casting = nil
    
    -- target info
    Helper.target = {}
    Helper.target.lunaire = false
    Helper.target.solaire = false
    Helper.target.casting = false
    
    -- spells
    Helper.spell = {}
    Helper.spell.courroux = "Courroux solaire"
    Helper.spell.lunaire = "Éclat lunaire(Lunaire)"
    Helper.spell.solaire = "Éclat solaire(Solaire)"
    Helper.spell.ecorse = "Ecorce"
    Helper.spell.eruption = "Éruption stellaire"
    Helper.spell.frappe = "Frappe lunaire"
    Helper.spell.meteores = "Météores"
    Helper.spell.rayon = "Rayon solaire"
    
    Helper.spell.guerrier = "Guerrier d’Elune"
    
    -- functions
    Helper.player_info = balance_player_info
    Helper.target_info = balance_target_info
    Helper.select_spell = balance_select_spell
    Helper.select_cut = balance_select_cut
end
