local function empty_player_info()
end

local function empty_target_info()
end

local function empty_select_spell()
	return nil
end

local function empty_select_cut()
	return nil
end

local function empty_select_focus_cut()
	return nil
end

function Helper:empty_init()
	Helper.player = {}
	Helper.target = {}
	Helper.spell  = {}
	
	Helper.player_info      = empty_player_info
	Helper.target_info      = empty_target_info
	Helper.select_spell     = empty_select_spell
	Helper.select_cut       = empty_select_cut
	Helper.select_focus_cut = empty_select_cut
end
