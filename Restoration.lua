local function restoration_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- energy
    Helper.player.energy = UnitPower("player", Enum.PowerType.Energy)
    -- Combo
    Helper.player.combo = UnitPower("player", Enum.PowerType.ComboPoints)
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("player")
    if name then
        Helper.player.casting = name
    else
        Helper.player.casting = nil
    end
end

local function restoration_target_info()
    
    -- Eclat lunaire
    local name, _, exp = Helper:UnitDebuff("target", "Éclat lunaire")
    if name == nil or exp < 2.0 then
        Helper.target.lunaire = false
    else
        Helper.target.lunaire = true
    end
    
    -- Solaire
    local name, _, exp = Helper:UnitDebuff("target", "Éclat solaire")
    if name == nil or exp < 2.0 then
        Helper.target.solaire = false
    else
        Helper.target.solaire = true
    end
    
    -- Griffure
    local name, _, exp = Helper:UnitDebuff("target", "Griffure")
    if name == nil then
        Helper.target.griffure = 0
    else
        Helper.target.griffure = exp
    end
    
    -- Déchirure
    local name, _, exp = Helper:UnitDebuff("target", "Déchirure")
    if name == nil then
        Helper.target.dechirure = 0
    else
        Helper.target.dechirure = exp
    end

    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
end

local function restoration_select_spell()
    local health = Helper.player.health
    local lunaire = Helper.target.lunaire
    local solaire = Helper.target.solaire
    local casting = Helper.player.casting
    local energy = Helper.player.energy
    local combo = Helper.player.combo
    local griffure = Helper.target.griffure
    
    -- print("     -----")
    -- print("health    : " .. tostring(health))
    -- print("lunaire   : " .. tostring(lunaire))
    -- print("solaire   : " .. tostring(solaire))
    -- print("lunaire   : " .. tostring(renf_lunaire))
    -- print("oneth   : " .. tostring(oneth))
    -- print("meteores: " .. tostring(meteores))
    -- print("casting   : " .. tostring(casting))
    -- print("     -----")
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.25 then
        -- Ecorse
        local spell = Helper.spell.ecorse
        if Helper:usable_spell(spell) == true and spell ~= casting then
            return spell
        end
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------
    -- 1/ Not in Compobat -> courroux
    -- local spell = Helper.spell.courroux
    -- if Helper:usable_ranged_spell(spell) == true and not UnitAffectingCombat("player") then
    -- return spell
    -- end
    
    --[[
    -- Solaire
    local spell = Helper.spell.solaire
    if Helper:usable_ranged_spell(spell) == true and not solaire and spell ~= casting then
        return spell
    end
    --]]

    -- Lunaire
    local spell = Helper.spell.lunaire
    if Helper:usable_ranged_spell(spell) == true and not lunaire and spell ~= casting then
        return spell
    end
    
    local spell = Helper.spell.griffure
    if Helper:usable_ranged_spell(spell) == true and griffure <= 2 then
        return spell
    end
    
    local spell = Helper.spell.morsure
    -- if Helper:usable_ranged_spell(spell) == true and combo >= 5 and dechirure >= 5 then
    if Helper:usable_ranged_spell(spell) == true and combo >= 5 then
        return spell
    end

    local spell = Helper.spell.lambeau
    if Helper:usable_spell(spell) == true and energy >= 35 then
        return spell
    end

    --[[
    local spell = Helper.spell.balayage
    if Helper:usable_spell(spell) == true and energy >= 35 then
        return spell
    end

    -- Courroux
    local spell = Helper.spell.courroux
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    --]]
    
    return nil
end

local function restoration_select_cut()
    --[[
    local cut = Helper.spell.rayon
    local casting = Helper.target.casting
    
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
    --]]
    return nil
end

function Helper:restoration_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.casting = nil
    --
    Helper.player.energy = 0
    Helper.player.combo = 0
    
    -- target info
    Helper.target = {}
    Helper.target.lunaire = false
    Helper.target.solaire = false
    Helper.target.casting = false
    --
    Helper.target.griffure = 0
    Helper.target.dechirure = 0
    
    -- spells
    Helper.spell = {}
    Helper.spell.lunaire = "Éclat lunaire(Lunaire)"
    Helper.spell.solaire = "Éclat solaire(Solaire)"
    Helper.spell.courroux = "Courroux solaire"
    Helper.spell.ecorse = "Ecorce"
    --     
    Helper.spell.balayage = "Balayage"
    Helper.spell.dechirure = "Déchirure"
    Helper.spell.griffure = "Griffure"
    Helper.spell.lambeau = "Lambeau"
    Helper.spell.morsure = "Morsure féroce"
    
    -- functions
    Helper.player_info = restoration_player_info
    Helper.target_info = restoration_target_info
    Helper.select_spell = restoration_select_spell
    Helper.select_cut = restoration_select_cut
end
