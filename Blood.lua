local function blood_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- Runic Power
    Helper.player.power = UnitPower("player")
    
    -- Runes
    Helper.player.runes = 0
    local max = UnitPowerMax("player", SPELL_POWER_RUNES)
    for runeIndex = 1, max do
        local start, duration, runeReady = GetRuneCooldown(runeIndex)
        if runeReady then
            Helper.player.runes = Helper.player.runes + 1
        end
    end
    
    -- Bouclie d'os
    local name, count, expirationTime = Helper:UnitBuff("player", "Bouclier d’os")
    if name == nil or count == 0 then
        Helper.player.bouclier = 0
    else
        if expirationTime < 3 then
            Helper.player.bouclier = 0
        else
            Helper.player.bouclier = count
        end
    end
    
    -- Fléau cramoisi
    local name, count, _ = Helper:UnitBuff("player", "Fléau cramoisi")
    if name == nil then
        Helper.player.fleau = false
    else
        Helper.player.fleau = true
    end
    
end

local function blood_target_info()
    
    -- Peste de sang
    local name, count, expirationTime = Helper:UnitDebuff("target", "Peste de sang")
    if name == nil then
        Helper.target.peste = false
    else
        if expirationTime < 2 then Helper.target.peste = false
        else Helper.target.peste = true
        end
    end
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
end

local function blood_select_spell()
    local health = Helper.player.health
    local power = Helper.player.power
    local runes = Helper.player.runes
    local bouclier = Helper.player.bouclier
    local fleau = Helper.player.fleau
    local peste = Helper.target.peste
    
    -- print("     -----")
    -- print("health   : " .. tostring(health))
    -- print("power    : " .. tostring(power))
    -- print("runes    : " .. tostring(runes))
    -- print("bouclier   : " .. tostring(bouclier))
    -- print("peste    : " .. tostring(peste))
    -- print("     -----")
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.75 then
        
        local spell = Helper.spell.buveur
        if Helper:usable_ranged_spell(spell) == true then
            return spell
        end
        
        local spell = Helper.spell.tempete
        if Helper:usable_spell(spell) == true and power >= 50 then
            return spell
        end
        
        local spell = Helper.spell.mort
        if Helper:usable_ranged_spell(spell) == true then
            return spell
        end
        
        local spell = Helper.spell.sang
        if Helper:usable_spell(spell) == true then
            return spell
        end
        
        if health < 0.5 then
            local spell = Helper.spell.robustesse
            if Helper:usable_spell(spell) == true then
                return spell
            end
        end
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------
    
    local spell = Helper.spell.dechirure
    if Helper:usable_ranged_spell(spell) == true and bouclier == 0 then
        return spell
    end
    
    local spell = Helper.spell.tempete
    if Helper:usable_spell(spell) == true and power >= 100 then
        return spell
    end
    
    local spell = Helper.spell.mort
    if Helper:usable_ranged_spell(spell) == true and power >= 100 then
        return spell
    end
    
    local spell = Helper.spell.buveur
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.furoncle
    if Helper:usable_spell(spell) == true and peste == false then
        return spell
    end
    
    local spell = Helper.spell.dechirure
    if Helper:usable_ranged_spell(spell) == true and bouclier <= 7 then
        return spell
    end
    
    local spell = Helper.spell.decomposition
    if Helper:usable_spell(spell) == true and fleau == true then
        return spell
    end
    
    local spell = Helper.spell.coeur
    if Helper:usable_ranged_spell(spell) == true and runes >= 3 then
        return spell
    end
    
    local spell = Helper.spell.furoncle
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    return nil
end

local function blood_select_cut()
    local cut = Helper.spell.gel
    local casting = Helper.target.casting
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

function Helper:blood_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.power = 0
    Helper.player.runes = 0
    Helper.player.bouclier = 0
    Helper.player.fleau = false
    
    -- target info
    Helper.target = {}
    Helper.target.peste = false
    Helper.target.casting = false
    
    -- spells
    Helper.spell = {}
    Helper.spell.dechirure = "Déchirure de moelle"
    Helper.spell.furoncle = "Furoncle sanglant"
    Helper.spell.coeur = "Frappe au cœur"
    Helper.spell.mort = "Frappe de mort"
    Helper.spell.decomposition = "Mort et décomposition"
    Helper.spell.buveur = "Buveur de sang"
    Helper.spell.tempete = "Tempête d’os"
    
    Helper.spell.sang = "Sang vampirique"
    Helper.spell.robustesse = "Robustesse glaciale"
    
    Helper.spell.gel = "Gel de l'esprit"
    
    -- functions
    Helper.player_info = blood_player_info
    Helper.target_info = blood_target_info
    Helper.select_spell = blood_select_spell
    Helper.select_cut = blood_select_cut
end
