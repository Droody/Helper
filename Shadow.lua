local function shadow_player_info()

	-- Health
	Helper.player.health = UnitHealth("player") / UnitHealthMax("player")

	-- Power
	Helper.player.power = UnitPower("player")

	-- Eclat lunaire
	local name, _, _ = Helper:UnitBuff("player", "Forme du vide")
	if name == nil then
		Helper.player.vide = false
	else
		Helper.player.vide = true
	end

	-- Casting
	local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo("player")
	if spell then
		if string.find(spell, "lune") then
			Helper.player.casting = Helper.spell.lune
		else
			Helper.player.casting = spell
		end
	else
		Helper.player.casting = nil
	end
end

local function shadow_target_info()
	local now = GetTime()

	-- Douleur
	local name, _, exp = Helper:UnitDebuff("target", "Mot de l’ombre : Douleur")
	if name == nil or exp < 2.0 then
		Helper.target.douleur = false
	else
		Helper.target.douleur = true
	end

	-- Douleur
	local name, _, exp = Helper:UnitDebuff("target", "Toucher vampirique")
	if name == nil or exp < 2.0 then
		Helper.target.toucher = false
	else
		Helper.target.toucher = true
	end

	-- Casting
	local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo("target")
	if spell ~= nil and interrupt == false then
		Helper.target.casting = true
	else
		Helper.target.casting = false
	end
end

local function shadow_select_spell()
	local health  = Helper.player.health
	local power   = Helper.player.power
	local vide    = Helper.player.vide
	local douleur = Helper.target.douleur
	local toucher = Helper.target.toucher
	local casting = Helper.player.casting

	-- -------------------------
	-- Heal phase
	-- -------------------------
	if health < 0.25 then
		-- TODO
	end

	-- -------------------------
	-- DPS phase : Forme du Vide
	-- -------------------------
	if vide == true then
		-- 1/ Eruption
		local spell = Helper.spell.eruption
		if Helper:usable_spell(spell) == true then 
			return spell
		end
		-- 2/ Attaque Mentale
		spell = Helper.spell.attaque
		if Helper:usable_ranged_spell(spell) == true then 
			return spell
		end
		-- 3/ Arme
		spell = Helper.spell.arme
		if Helper:usable_ranged_spell(spell) == true then 
			return spell
		end
		-- 4/ Douleur
		spell = Helper.spell.douleur
		if Helper:usable_ranged_spell(spell) == true and douleur == false then 
			return spell
		end
		-- 5/ Toucher
		spell = Helper.spell.toucher
		if Helper:usable_ranged_spell(spell) == true and toucher == false then 
			return spell
		end
		-- 6/ Ombrefiel
		spell = Helper.spell.ombrefiel
		if Helper:usable_ranged_spell(spell) == true then 
			return spell
		end
		-- 7/ Fouet
		spell = Helper.spell.fouet
		if Helper:usable_ranged_spell(spell) == true then 
			return spell
		end
	-- -------------------------
	-- DPS phase : Forme d'ombre
	-- -------------------------
	else
		-- 1/ Eruption
		local spell = Helper.spell.eruption
		if Helper:usable_spell(spell) == true then 
			return spell
		end
		-- 2/ Attaque Mentale
		spell = Helper.spell.attaque
		if Helper:usable_ranged_spell(spell) == true then 
			return spell
		end
		-- 3/ Mort
		spell = Helper.spell.mort
		if Helper:usable_spell(spell) == true then 
			return spell
		end
		-- 4/ Douleur
		spell = Helper.spell.douleur
		if Helper:usable_ranged_spell(spell) == true and douleur == false then 
			return spell
		end
		-- 5/ Toucher
		spell = Helper.spell.toucher
		if Helper:usable_ranged_spell(spell) == true and toucher == false then 
			return spell
		end
		-- 6/ Fouet
		spell = Helper.spell.fouet
		if Helper:usable_ranged_spell(spell) == true then 
			return spell
		end
	end

	return nil
end

local function shadow_select_cut()
	local cut = Helper.spell.silence
	local casting = Helper.target.casting
	if casting == true and Helper:usable_ranged_spell(cut) == true then
		return cut
	else
		return nil
	end
end

function Helper:shadow_init()
	-- player info
	Helper.player = {}
	Helper.player.health  = 1.0
	Helper.player.power   = 1.0
	Helper.player.vide    = 0
	Helper.player.casting = nil

	-- target info
	Helper.target = {}
	Helper.target.douleur = false
	Helper.target.toucher = false

	-- spells
	Helper.spell = {}
	Helper.spell.attaque   = "Attaque mentale"
	Helper.spell.eruption  = "Eruption du Vide"
	Helper.spell.etreinte  = "Etreinte vampirique"
	Helper.spell.explosion = "Explosion mentale"
	Helper.spell.fouet     = "Fouet mental"
	Helper.spell.guerison  = "Guérison de l’ombre"
	Helper.spell.mort      = "Mot de l'ombre : Mort"
	Helper.spell.douleur   = "Mot de l’ombre : Douleur"
	Helper.spell.bouclier  = "Mot de pouvoir : Bouclier"
	Helper.spell.ombrefiel = "Ombrefiel"
	Helper.spell.silence   = "Silence"
	Helper.spell.toucher   = "Toucher vampirique"
	Helper.spell.arme      = "Torrent du Vide(Arme prodigieuse)"

	-- functions
	Helper.player_info  = shadow_player_info
	Helper.target_info  = shadow_target_info
	Helper.select_spell = shadow_select_spell
	Helper.select_cut   = shadow_select_cut
end
