local function unholy_player_info()

	-- Health
	Helper.player.health = UnitHealth("player") / UnitHealthMax("player")

	-- Runic Power
	Helper.player.power   = UnitPower("player")

	-- Runes
	Helper.player.runes = 0
	local max = UnitPowerMax("player", SPELL_POWER_RUNES)
	for runeIndex = 1, max do
		local start, duration, runeReady = GetRuneCooldown(runeIndex)
		if runeReady then
			Helper.player.runes = Helper.player.runes + 1
		end
	end
	
	-- Frimas
	local name, rank, icon, count, debuffType, duration, expirationTime, unitCaster, isStealable, shouldConsolidate, spellId = UnitBuff("player", "Malédiction soudaine")
	if name == nil then
		Helper.player.malediction = false
	else
		local exp = expirationTime - GetTime()
		if exp < 1 then
			Helper.player.malediction = false
		else
			Helper.player.malediction = true
		end
	end

end

local function unholy_target_info()

	-- Peste virulente
	local name, rank, icon, count, debuffType, duration, expirationTime, unitCaster, isStealable, shouldConsolidate, spellId = UnitDebuff("target", "Peste virulente", nil , "player")
	if name == nil then
		Helper.target.peste = false
	else
		local exp = expirationTime - GetTime()
		if exp < 2 then Helper.target.peste = false
		else Helper.target.peste = true
		end
	end

	-- Blessures purulentes
	local name, rank, icon, count, debuffType, duration, expirationTime, unitCaster, isStealable, shouldConsolidate, spellId = UnitDebuff("target", "Blessure purulente", nil , "player")
	if name == nil then
		Helper.target.blessures = 0
	else
		Helper.target.blessures = count
	end

	-- Casting
	local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo("target")
	if spell ~= nil and interrupt == false then
		Helper.target.casting = icon
	else
		Helper.target.casting = false
	end
end

local function unholy_select_spell()
	local health      = Helper.player.health
	local power       = Helper.player.power
	local runes       = Helper.player.runes
	local malediction = Helper.player.malediction

	local peste       = Helper.target.peste
	local blessures   = Helper.target.blessures

	-- print("     -----")
	-- print("health   : " .. tostring(health))
	-- print("power    : " .. tostring(power))
	-- print("runes    : " .. tostring(runes))
	-- print("bouclier   : " .. tostring(bouclier))
	-- print("peste    : " .. tostring(peste))
	-- print("     -----")

	-- -------------------------
	-- Heal phase
	-- -------------------------
	if health < 0.75 then

		local spell = Helper.spell.mort
		if Helper:usable_ranged_spell(spell) == true then 
			return spell
		end

		if health < 0.5 then
			local spell = Helper.spell.robustesse
			if Helper:usable_spell(spell) == true then 
				return spell
			end
		end
	end

	-- -------------------------
	-- DPS phase
	-- -------------------------

	local spell = Helper.spell.fievre
	if Helper:usable_spell(spell) == true and peste == false then 
		return spell
	end

	local spell = Helper.spell.transformation
	if Helper:usable_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.arme
	if Helper:usable_spell(spell) == true and blessures >= 6 then 
		return spell
	end

	local spell = Helper.spell.griffes
	if Helper:usable_spell(spell) == true and blessures >= 6 and Helper:usable_spell(Helper.spell.arme) == false then 
		return spell
	end

	local spell = Helper.spell.voile
	if Helper:usable_spell(spell) == true and power >= 80 then 
		return spell
	end

	local spell = Helper.spell.profanation
	if Helper:usable_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.frappe
	if Helper:usable_ranged_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.griffes
	if Helper:usable_spell(spell) == true and blessures >= 3 and Helper:usable_spell(Helper.spell.arme) == false then 
		return spell
	end

	local spell = Helper.spell.voile
	if Helper:usable_spell(spell) == true and power >= 60 then 
		return spell
	end

	local spell = Helper.spell.voile
	if Helper:usable_spell(spell) == true and malediction == true then 
		return spell
	end

	return nil
end

local function unholy_select_cut()
	local cut = Helper.spell.gel
	local casting = Helper.target.casting
	if casting ~= false and Helper:usable_ranged_spell(cut) == true then
		return casting
	else
		return nil
	end
end

function Helper:unholy_init()
	-- player info
	Helper.player = {}
	Helper.player.health      = 1.0
	Helper.player.power       = 0
	Helper.player.runes       = 0
	Helper.player.malediction = false

	-- target info
	Helper.target = {}
	Helper.target.preste    = false
	Helper.target.blessures = 0
	Helper.target.casting   = false

	-- spells
	Helper.spell = {}


	Helper.spell.frappe         = "Frappe purulente"
	Helper.spell.fievre         = "Poussée de fièvre"
	Helper.spell.transformation = "Sombre transformation"
	Helper.spell.voile          = "Voile mortel"

	Helper.spell.arme           = "Apocalypse(Arme prodigieuse)"
	Helper.spell.griffes        = "Griffes des ombres"
	Helper.spell.profanation    = "Profanation"

	Helper.spell.mort           = "Frappe de mort"
	Helper.spell.robustesse     = "Robustesse glaciale"

	Helper.spell.gel            = "Gel de l'esprit"
	
	-- functions
	Helper.player_info  = unholy_player_info
	Helper.target_info  = unholy_target_info
	Helper.select_spell = unholy_select_spell
	Helper.select_cut   = unholy_select_cut
end
