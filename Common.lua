function Helper:UnitBuff(uid, spell)
    for i = 1, 40 do
        local name, _, count, _, _, expirationTime, _ = UnitBuff(uid, i, "PLAYER")
        if name == spell then
            local exp = expirationTime - GetTime()
            return name, count, exp
        end
    end
    return nil
end

function Helper:UnitDebuff(uid, spell)
    for i = 1, 40 do
        local name, _, count, _, _, expirationTime, _ = UnitDebuff(uid, i, "PLAYER")
        if name == spell then
            local exp = expirationTime - GetTime()
            return name, count, exp
        end
    end
    return nil
end

function Helper:create_main_frame()
    local width = Helper.width
    local height = Helper.height

    local frame = CreateFrame("Frame", "Helper", UIParent)
    frame:SetFrameStrata("MEDIUM")
    frame:SetFrameLevel(0)

    frame:SetMovable(true)
    frame:EnableMouse(true)
    frame:RegisterForDrag("LeftButton")
    frame:SetScript("OnDragStart", frame.StartMoving)
    frame:SetScript("OnDragStop", frame.StopMovingOrSizing)

    frame:SetWidth(width)
    frame:SetHeight(height)

    frame.texture = frame:CreateTexture()
    frame.texture:SetAllPoints(frame)
    frame.texture:SetColorTexture(1.0, 1.0, 1.0, 0)
    frame:SetPoint("CENTER", 0, -150)

    Helper.frame = frame
end

function Helper:create_cd_frame()
    local width = Helper.cd_width
    local height = Helper.cd_height

    local frame = CreateFrame("Frame", "Helper_CD", Helper.frame)
    frame:SetFrameStrata("MEDIUM")
    frame:SetFrameLevel(0)

    frame:SetMovable(false)

    frame:SetPoint("TOPRIGHT", -Helper.width, 0)
    frame:SetWidth(width)
    frame:SetHeight(height)

    frame.texture = frame:CreateTexture()
    frame.texture:SetAllPoints(frame)
    frame.texture:SetColorTexture(0.0, 1.0, 0.0, Helper.alpha)

    Helper.cd_frame = frame
end

function Helper:create_cut_frame()
    local width = Helper.width
    local height = Helper.height

    local frame = CreateFrame("Frame", "Helper_CUT", Helper.frame)
    frame:SetFrameStrata("MEDIUM")
    frame:SetFrameLevel(0)

    frame:SetMovable(false)

    frame:SetPoint("TOPRIGHT", width, 0)
    frame:SetWidth(width)
    frame:SetHeight(height)

    frame.texture = frame:CreateTexture()
    frame.texture:SetAllPoints(frame)
    frame.texture:SetColorTexture(1.0, 1.0, 1.0, Helper.alpha)

    Helper.cut_frame = frame
end

function Helper:create_focus_cut_frame()
    local width = Helper.width
    local height = Helper.height

    local frame = CreateFrame("Frame", "Helper_focus_CUT", Helper.frame)
    frame:SetFrameStrata("MEDIUM")
    frame:SetFrameLevel(0)

    frame:SetMovable(false)

    frame:SetPoint("TOPRIGHT", 2 * width, 0)
    frame:SetWidth(width)
    frame:SetHeight(height)

    frame.texture = frame:CreateTexture()
    frame.texture:SetAllPoints(frame)
    frame.texture:SetColorTexture(1.0, 1.0, 1.0, Helper.alpha)

    Helper.focus_cut_frame = frame
end

function Helper:update_frame(spell)
    local frame = Helper.frame
    if spell == nil then
        -- local _, _, icon = GetSpellInfo(spell)
        frame.texture:SetTexture("Interface/ICONS/INV_Misc_QuestionMark")
        frame.texture:SetAllPoints(frame)
    else
        local _, _, icon = GetSpellInfo(spell)
        frame.texture:SetTexture(icon)
        frame.texture:SetAllPoints(frame)
        frame.texture:SetAlpha(Helper.alpha)
    end
end

function Helper:update_cd_frame(spell)
    local frame = Helper.cd_frame
    if spell == nil then
        -- frame:SetWidth(1)
        frame:Hide()
    else
        local start, duration = GetSpellCooldown(spell)
        if start == nil or duration == 0 then
            -- frame:SetWidth(1)
            frame:Hide()
        else
            local d = start + duration - GetTime()
            local width = Helper.cd_width * d
            frame:SetWidth(width)
            if d < 0.5 then
                frame.texture:SetColorTexture(1.0, 0.0, 0.0, Helper.alpha)
            elseif d < 1.0 then
                frame.texture:SetColorTexture(1.0, 1.0, 0.0, Helper.alpha)
            else
                frame.texture:SetColorTexture(0.0, 1.0, 0.0, Helper.alpha)
            end
            frame:Show()
        end
    end
end

function Helper:update_cut_frame(spell)
    local frame = Helper.cut_frame
    if spell == nil then
        frame:Hide()
    else
        -- local _, _, icon = GetSpellInfo(spell)
        frame.texture:SetTexture(spell)
        frame.texture:SetAllPoints(frame)
        frame.texture:SetAlpha(Helper.alpha)
        frame:Show()
    end
end

function Helper:update_focus_cut_frame(spell)
    local frame = Helper.focus_cut_frame
    if spell == nil then
        frame:Hide()
    else
        -- local _, _, icon = GetSpellInfo(spell)
        frame.texture:SetTexture(spell)
        frame.texture:SetAllPoints(frame)
        frame.texture:SetAlpha(Helper.alpha)
        frame:Show()
    end
end

function Helper:usable_spell(spell)
    if UnitIsDead("target") then
        return false
    elseif not IsUsableSpell(spell) then
        return false
    else
        local start, duration = GetSpellCooldown(spell)
        if start == 0 then
            return true
        elseif duration then
            local _, gcd = GetSpellCooldown(61304)
            local d = start + duration - GetTime()
            if d and gcd and d <= gcd then
                return true
            else
                return false
            end
        end
    end
end

function Helper:usable_ranged_spell(spell)
    local range = IsSpellInRange(spell, "target")
    if range ~= 1 then
        -- print("range: " .. spell .. " = " .. tostring(range))
        return false
    else
        return Helper:usable_spell(spell)
    end
end

function Helper:usable_ranged_focus_spell(spell)
    -- TODO
    return true
    -- return Helper:usable_spell(spell)
    -- if IsSpellInRange(spell, "focus") ~= 1 then return false
    -- else return Helper:usable_spell(spell)
    -- end
end
