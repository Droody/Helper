local function protection_player_info()

	-- Health
	Helper.player.health = UnitHealth("player") / UnitHealthMax("player")

	-- Consecration
	local name, _, exp = Helper:UnitBuff("player", "Consécration")
	if name == nil then
		Helper.player.consecration = false
	else
		if exp < 1 then
			Helper.player.consecration = false
		else
			Helper.player.consecration = true
		end
	end
end

local function protection_target_info()

    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end

end

local function protection_select_spell()
	local health       = Helper.player.health
	local consecration = Helper.player.consecration
	-- print("     -----")
	-- print("health   : " .. tostring(health))
	-- print("power    : " .. tostring(power))
	-- print("runes    : " .. tostring(runes))
	-- print("bouclier   : " .. tostring(bouclier))
	-- print("consecration    : " .. tostring(consecration))
	-- print("     -----")

	-- -------------------------
	-- Heal phase
	-- -------------------------
	if health < 0.75 then

		if health < 0.5 then
			local spell = Helper.spell.lumiere
			if Helper:usable_spell(spell) == true then 
				return spell
			end

			local spell = Helper.spell.divine
			if Helper:usable_spell(spell) == true then 
				return spell
			end
		end

	end

	-- -------------------------
	-- DPS phase
	-- -------------------------

	local spell = Helper.spell.consecration
	if Helper:usable_spell(spell) == true and consecration == false then 
		return spell
	end

	local spell = Helper.spell.jugement
	if Helper:usable_ranged_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.bouclier
	if Helper:usable_ranged_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.marteau
	if Helper:usable_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.consecration
	if Helper:usable_spell(spell) == true then 
		return spell
	end

	return nil
end

local function protection_select_cut()
	local cut = Helper.spell.reprimandes
	local casting = Helper.target.casting
	if casting ~= false and Helper:usable_ranged_spell(cut) == true then
		return casting
	else
		return nil
	end
end

function Helper:protection_init()
	-- player info
	Helper.player = {}
	Helper.player.health       = 1.0
	Helper.player.consecration = false

	-- target info
	Helper.target = {}
	Helper.target.peste  = false
	Helper.target.casting = false

	-- spells
	Helper.spell = {}

	Helper.spell.jugement     = "Jugement"
	Helper.spell.bouclier     = "Bouclier du vengeur"
	Helper.spell.consecration = "Consécration"
	Helper.spell.marteau      = "Marteau du vertueux"

	Helper.spell.divine       = "Protection divine"
	Helper.spell.lumiere      = "Lumière du protecteur"

	Helper.spell.reprimandes  = "Réprimandes"

	
	-- functions
	Helper.player_info  = protection_player_info
	Helper.target_info  = protection_target_info
	Helper.select_spell = protection_select_spell
	Helper.select_cut   = protection_select_cut
end
