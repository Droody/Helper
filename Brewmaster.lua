local function brewmaster_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- Pain
    Helper.player.energy = UnitPower("player")
    
    -- Oeil
    local name, _, _ = Helper:UnitBuff("player", "Oeil du tigre")
    if name == nil then
        Helper.player.oeil = false
    else
        Helper.player.oeil = true
    end
    
    -- Vent
    local name, _, _ = Helper:UnitBuff("player", "Vent de jade fulgurant")
    if name == nil then
        Helper.player.vent = false
    else
        Helper.player.vent = true
    end
    
    -- Fer
    local name, _, expirationTime = Helper:UnitBuff("player", "Infusion peau-de-fer")
    if name == nil then
        Helper.player.fer = 0
    else
        Helper.player.fer = expirationTime
    end
    
    -- Report
    Helper.player.report = UnitStagger("player")
    
end

local function brewmaster_target_info()
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
    
end

local function brewmaster_select_spell()
    local health = Helper.player.health
    local energy = Helper.player.energy
    local oeil = Helper.player.oeil
    local vent = Helper.player.vent
    local report = Helper.player.report
    local fer = Helper.player.fer
    
    -- print("     -----")
    -- print("health   : " .. tostring(health))
    -- print("pain     : " .. tostring(pain))
    -- print("fragments: " .. tostring(fragments))
    -- print("fragilite: " .. tostring(fragilite))
    -- print("report: " .. tostring(report))
    -- print("     -----")
    
    local spell = Helper.spell.vent
    if Helper:usable_spell(spell) == true and vent == false and not UnitAffectingCombat("player") then
        return spell
    end
        
    local spell = Helper.spell.infu_fer
    if Helper:usable_spell(spell) == true and GetSpellCharges(spell) > 0 and fer < 5 then
        return spell
    end
    
    --[[
    local spell = Helper.spell.infu_pur
    if Helper:usable_spell(spell) == true and GetSpellCharges(spell) >= 3 and report > 0 then
        return spell
    end
    --]]
    
    local spell = Helper.spell.infu_pur
    if Helper:usable_spell(spell) == true and GetSpellCharges(spell) > 1 and report >= (UnitHealthMax("player") / 10) then
        return spell
    end
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.75 then
        
        local spell = Helper.spell.infu_fer
        if Helper:usable_spell(spell) == true and fer <= 0 then
            return spell
        end
        
        local spell = Helper.spell.extraction
        if Helper:usable_spell(spell) == true then
            return spell
        end
        
        local spell = Helper.spell.boisson
        if Helper:usable_spell(spell) == true and health < 0.50 then
            return spell
        end
        
        --[[
        local spell = Helper.spell.meditation
        if Helper:usable_spell(spell) == true and health < 0.25 then
            return spell
        end
        ]]--
        
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------
    
    --
    --
    local spell = Helper.spell.fracasse
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.frappe
    -- if Helper:usable_ranged_spell(spell) == true then
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.souffle
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.vent
    if Helper:usable_spell(spell) == true and vent == false then
        return spell
    end
    
    --[[
    local spell = Helper.spell.infu_pur
    if Helper:usable_spell(spell) == true and GetSpellCharges(spell) >= 2 and report > 0 then
        return spell
    end
    --]]
    
    local spell = Helper.spell.paume
    if Helper:usable_ranged_spell(spell) == true and energy >= 50 then
        return spell
    end
    
    local spell = Helper.spell.infu_fer
    if Helper:usable_spell(spell) == true and GetSpellCharges(spell) > 1 and fer < 13 then
        return spell
    end
    
    return nil
end

local function brewmaster_select_cut()
    local cut = Helper.spell.pique
    local casting = Helper.target.casting
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

local function brewmaster_select_focus_cut()
    local cut = Helper.spell.pique
    local casting = Helper.focus.casting
    if casting ~= false and Helper:usable_ranged_focus_spell(cut) == true then
        return casting
    else
        return nil
    end
end

function Helper:brewmaster_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.energy = 0
    Helper.player.oeil = false
    Helper.player.vent = false
    Helper.player.report = 0
    Helper.player.fer = 0
    
    -- target info
    Helper.target = {}
    
    -- focus info
    Helper.focus = {}
    Helper.focus.casting = false
    
    -- spells
    Helper.spell = {}
    
    -- offence
    Helper.spell.fracasse = "Fracasse-tonneau"
    Helper.spell.frappe = "Frappe aveuglante"
    Helper.spell.souffle = "Souffle de feu"
    Helper.spell.vent = "Vent de jade fulgurant"
    Helper.spell.paume = "Paume du tigre"
    
    -- defence
    Helper.spell.infu_fer = "Infusion peau-de-fer"
    Helper.spell.infu_pur = "Infusion purificatrice"
    Helper.spell.extraction = "Extraction du mal"
    Helper.spell.boisson = "Boisson fortifiante"
    Helper.spell.meditation = "Méditation zen"
    
    -- cut
    Helper.spell.pique = "Pique de main"
    
    -- functions
    Helper.player_info = brewmaster_player_info
    Helper.target_info = brewmaster_target_info
    Helper.select_spell = brewmaster_select_spell
    Helper.select_cut = brewmaster_select_cut
    Helper.select_focus_cut = brewmaster_select_focus_cut
end
