local function mistweaver_player_info()

	-- Health
	Helper.player.health = UnitHealth("player") / UnitHealthMax("player")

	-- mane
	Helper.player.mana = UnitPower("player")

end

local function mistweaver_target_info()

	-- Casting
	local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo("target")
	if spell ~= nil and interrupt == false then
		Helper.target.casting = icon
	else
		Helper.target.casting = false
	end

	-- Focus Casting
	local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo("focus")
	if spell ~= nil then
		Helper.focus.casting = icon
	else
		Helper.focus.casting = false
	end

end

local function mistweaver_select_spell()
	local health = Helper.player.health
	local mana = Helper.player.mana

	-- print("     -----")
	-- print("health   : " .. tostring(health))
	-- print("pain     : " .. tostring(pain))
	-- print("fragments: " .. tostring(fragments))
	-- print("fragilite: " .. tostring(fragilite))
	-- print("chi   : " .. tostring(chi))
	-- print("energy: " .. tostring(energy))
	-- print("     -----")

	-- -------------------------
	-- Heal phase
	-- -------------------------
	if health < 0.75 then
		--[[ 
		local spell = Helper.spell.extraction
		if Helper:usable_spell(spell) == true then 
			return spell
		end

		local spell = Helper.spell.boisson
		if Helper:usable_spell(spell) == true and health < 0.50 then 
			return spell
		end

		local spell = Helper.spell.meditation
		if Helper:usable_spell(spell) == true and health < 0.25 then 
			return spell
		end
		--]]

	end

	-- -------------------------
	-- DPS phase
	-- -------------------------

	local spell = Helper.spell.soleil
	if Helper:usable_ranged_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.voile
	if Helper:usable_ranged_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.tigre
	if Helper:usable_ranged_spell(spell) == true then 
		return spell
	end

	return nil
end

local function mistweaver_select_cut()
	local cut = Helper.spell.paralysie
	local casting = Helper.target.casting
	if casting ~= false and Helper:usable_ranged_spell(cut) == true then
		return casting
	else
		return nil
	end
end

local function mistweaver_select_focus_cut()
	local cut = Helper.spell.paralysie
	local casting = Helper.focus.casting
	if casting ~= false and Helper:usable_ranged_focus_spell(cut) == true then
		return casting
	else
		return nil
	end
end

function Helper:mistweaver_init()
	-- player info
	Helper.player = {}
	Helper.player.health = 1.0
	Helper.player.mana = 0

	-- target info
	Helper.target = {}
	Helper.target.casting = false

	-- focus info
	Helper.focus = {}
	Helper.focus.casting = false

	-- spells
	Helper.spell = {}
	Helper.spell.soleil = "Coup de pied du soleil levant"
	Helper.spell.voile  = "Frappe du voile noir"
	Helper.spell.tigre  = "Paume du tigre"

	-- talents
	
	-- Def
	Helper.spell.infu_pur   = "Toucher du karma"
	Helper.spell.extraction = "Élixir de soins"

	-- cut
	Helper.spell.paralysie = "Paralysie"

	-- functions
	Helper.player_info      = mistweaver_player_info
	Helper.target_info      = mistweaver_target_info
	Helper.select_spell     = mistweaver_select_spell
	Helper.select_cut       = mistweaver_select_cut
	Helper.select_focus_cut = mistweaver_select_focus_cut
end
