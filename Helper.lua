local function select_helper()
    local _, class, _ = UnitClass("player")
    local specID = GetSpecialization()

    -- print(class)
    -- print(tostring(specID))

    --
    -- -------------------- DH --------------------
    -- Havoc
    if class == "DEMONHUNTER" and specID == 1 then
        -- Vengeance
        print("Helper: Havoc")
        Helper:havoc_init()
    elseif class == "DEMONHUNTER" and specID == 2 then
        -- -------------------- Monk --------------------
        -- Brewmaster
        --
        print("Helper: Vengeance")
        Helper:vengeance_init()
    elseif class == "MONK" and specID == 1 then
        print("Helper: Brewmaster")
        Helper:brewmaster_init()
    elseif class == "MONK" and specID == 2 then
        print("Helper: Mistweaver")
        Helper:mistweaver_init()
    elseif class == "MONK" and specID == 3 then
        -- -------------------- DK --------------------
        -- Blood
        -- Windwalker
        -- print("Helper: Empty")
        -- Helper:empty_init()
        --
        print("Helper: Windwalker")
        Helper:windwalker_init()
    elseif class == "DEATHKNIGHT" and specID == 1 then
        -- Frost
        print("Helper: Blood")
        Helper:blood_init()
    elseif class == "DEATHKNIGHT" and specID == 2 then
        -- Unholy
        print("Helper: Frost")
        -- Helper:frost_init()
        print("Helper: Empty")
        Helper:empty_init()
    elseif class == "DEATHKNIGHT" and specID == 3 then
        --
        -- -------------------- Paladin --------------------
        -- Protection
        print("Helper: Unholy")
        --Helper:unholy_init()
        print("Helper: Empty")
        Helper:empty_init()
    elseif class == "PALADIN" and specID == 2 then
        -- Protection
        print("Helper: Protection")
        Helper:protection_init()
    elseif class == "PALADIN" and specID == 3 then
        --
        -- -------------------- Drood --------------------
        -- Balance
        print("Helper: Retribution")
        Helper:retribution_init()
    elseif class == "DRUID" and specID == 1 then
        -- Feral
        print("Helper: Balance")
        Helper:balance_init()
    elseif class == "DRUID" and specID == 2 then
        -- Guardian
        print("Helper: Feral")
        Helper:feral_init()
    elseif class == "DRUID" and specID == 3 then
        -- Restoration
        print("Helper: Guardian")
        Helper:guardian_init()
    elseif class == "DRUID" and specID == 4 then
        --
        -- -------------------- Priest --------------------
        -- Shadow
        print("Helper: Retosration")
        Helper:restoration_init()
    elseif class == "PRIEST" and specID == 3 then
        --
        -- -------------------- Shaman --------------------
        -- Balance
        print("Helper: Shadow")
        Helper:shadow_init()
    elseif class == "SHAMAN" and specID == 1 then
        -- Balance
        print("Helper: Elementary")
        Helper:empty_init()
    elseif class == "SHAMAN" and specID == 2 then
        -- Balance
        print("Helper: Enhancement")
        Helper:enhancement_init()
    elseif class == "SHAMAN" and specID == 3 then
        --
        -- -------------------- Hunter --------------------
        --
        print("Helper: Restoration")
        Helper:empty_init()
    elseif class == "HUNTER" and specID == 1 then
        --
        print("Helper: ???")
        print("Helper: Empty")
        Helper:empty_init()
    elseif class == "HUNTER" and specID == 2 then
        -- Balance
        print("Helper: ???")
        print("Helper: Empty")
        Helper:empty_init()
    elseif class == "HUNTER" and specID == 3 then
        --
        -- -------------------- Warrior --------------------
        --
        print("Helper: Survival")
        Helper:survival_init()
    elseif class == "WARRIOR" and specID == 1 then
        --
        print("Helper: Arm")
        print("Helper: Empty")
        Helper:empty_init()
    elseif class == "WARRIOR" and specID == 2 then
        -- Balance
        print("Helper: Fury")
        Helper:fury_init()
    elseif class == "WARRIOR" and specID == 3 then
        --
        -- -------------------- --------------------
        -- Empty
        print("Helper: Protection")
        print("Helper: Empty")
        Helper:empty_init()
    else
        print("Helper: Empty")
        Helper:empty_init()
    end

    Helper.frame:Show()
    Helper.cd_frame:Show()
end

local function update()
    local now = GetTime()

    if Helper.next <= now then
        Helper.player_info()
        Helper.target_info()
        -- Spell
        local spell = Helper.select_spell()
        Helper:update_frame(spell)
        Helper:update_cd_frame(spell)
        -- Cut
        local cut = Helper.select_cut()
        Helper:update_cut_frame(cut)
        -- Focus Cut
        local focus_cut = Helper.select_focus_cut()
        Helper:update_focus_cut_frame(focus_cut)
        -- Next
        Helper.next = GetTime() + Helper.delay
    end
end

HelperSettings = HelperSettings or {}
Helper:empty_init()
Helper:create_main_frame()
Helper:create_cd_frame()
Helper:create_cut_frame()
Helper:create_focus_cut_frame()

Helper.frame:RegisterEvent("PLAYER_ENTERING_WORLD")
Helper.frame:RegisterEvent("PLAYER_TALENT_UPDATE")

local function eventHandler(self, event, ...)
    if event == "PLAYER_LOGIN" then
        if HelperSettings.XPos then
            Helper.x = HelperSettings.XPos
        end
        if HelperSettings.YPos then
            Helper.y = HelperSettings.YPos
        end
    elseif event == "PLAYER_ENTERING_WORLD" then
        select_helper()
    elseif event == "PLAYER_TALENT_UPDATE" then
        select_helper()
    end
end

Helper.frame:SetScript("OnEvent", eventHandler)
Helper.frame:SetScript("OnUpdate", update)
