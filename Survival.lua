local function survival_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- Power
    Helper.player.power = UnitPower("player")
    
    -- Assaut coordonné
    local name, _, exp = Helper:UnitBuff("player", "Assaut coordonné")
    if name == nil then
        Helper.player.assaut = 0
    else
        Helper.player.assaut = exp
    end
    
    --[[
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("player")
    if name then
        Helper.player.casting = name
    else
        Helper.player.casting = nil
    end
    --]]
end

local function survival_target_info()
    
    local name, _, exp = Helper:UnitDebuff("target", "Morsure de serpent")
    if name == nil then
        Helper.target.serpent = 0
    else
        Helper.target.serpent = exp
    end

    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
end

local function survival_select_spell()
    local health = Helper.player.health
    local power = Helper.player.power
    local assaut = Helper.player.assaut
    local serpent = Helper.target.serpent
    
    -- print("     -----")
    -- print("health    : " .. tostring(health))
    -- print("lunaire   : " .. tostring(lunaire))
    -- print("assaut      : " .. tostring(assaut))
    -- print("givre   : " .. tostring(givre))
    -- print("lunaire   : " .. tostring(renf_lunaire))
    -- print("oneth   : " .. tostring(oneth))
    -- print("meteores: " .. tostring(meteores))
    -- print("casting   : " .. tostring(casting))
    -- print("     -----")
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.30 then
        local spell = Helper.spell.enthousiasme
        if Helper:usable_spell(spell) == true then
            return spell
        end
        local spell = Helper.spell.tortue
        if Helper:usable_spell(spell) == true then
            return spell
        end
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------

    --[[
    local spell = Helper.spell.raptor
    if Helper:usable_ranged_spell(spell) == true and assaut > 0 and assaut <= 5 then
        return spell
    end
    --]]
    
    local spell = Helper.spell.harpon
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.assaut
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.serpent
    if Helper:usable_ranged_spell(spell) == true and serpent < 2  then
        return spell
    end
    
    local spell = Helper.spell.bombe
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.raptor
    if Helper:usable_ranged_spell(spell) == true and power > 70 then
        return spell
    end
    
    local spell = Helper.spell.tuer
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.aigle
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.raptor
    if Helper:usable_ranged_spell(spell) == true and power > 10 then
        return spell
    end
    
    return nil
end

local function survival_select_cut()
    local cut = Helper.spell.museliere
    local casting = Helper.target.casting
    
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

function Helper:survival_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.power = 1.0
    Helper.player.assaut = 0
    
    -- target info
    Helper.target = {}
    Helper.target.serpent = 0
    
    -- spells
    Helper.spell = {}
    Helper.spell.assaut = "Assaut coordonné"
    Helper.spell.serpent = "Morsure de serpent"
    Helper.spell.bombe = "Bombe de feu de brousse"
    Helper.spell.raptor = "Attaque du raptor"
    Helper.spell.tuer = "Ordre de tuer"
    Helper.spell.aigle = "Aspect de l’aigle"
    
    Helper.spell.harpon = "Harpon"

    Helper.spell.museliere = "Muselière"
    
    Helper.spell.enthousiasme = "Enthousiasme"
    Helper.spell.tortue = "Aspect de la tortue"
    
    -- functions
    Helper.player_info = survival_player_info
    Helper.target_info = survival_target_info
    Helper.select_spell = survival_select_spell
    Helper.select_cut = survival_select_cut
end
