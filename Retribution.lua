local function retribution_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- Mana
    -- Helper.player.mana = UnitPower("player")
    
    -- Power
    Helper.player.power = UnitPower("player", Enum.PowerType.HolyPower)

    --[[
    -- Fragments
    local name, count, _ = Helper:UnitBuff("player", "Fragments d’âme")
    if name == nil or count == 0 then
        Helper.player.fragments = 0
    else
        Helper.player.fragments = count
    end
    
    -- Pointes
    local name, _, _ = Helper:UnitBuff("player", "Pointes démoniaques")
    if name == nil then
        Helper.player.pointes = false
    else
        Helper.player.pointes = true
    end
--]]
end

local function retribution_target_info()
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
    
    --[[
    -- Fragilite
    local name, _, exp = Helper:UnitDebuff("target", "Fragilité", nil, "player")
    if name == nil then
        Helper.target.fragilite = false
    else
        if exp < 5 then Helper.target.fragilite = false
        else Helper.target.fragilite = true
        end
    end
    
    -- Marque
    local name, _, _ = Helper:UnitDebuff("target", "Marque enflammée")
    if name == nil then
        Helper.target.marque = false
    else
        Helper.target.marque = true
    end
    
    -- Sigil
    local name, _, _ = Helper:UnitDebuff("target", "Sigil de feu")
    if name == nil then
        Helper.target.sigil = false
    else
        Helper.target.sigil = true
    end
--]]
end

local function retribution_select_spell()
    local health = Helper.player.health
    local power = Helper.player.power
    
    -- print("     -----")
    -- print("health   : " .. tostring(health))
    -- print("power     : " .. tostring(power))
    -- print("fragments: " .. tostring(fragments))
    -- print("fragilite: " .. tostring(fragilite))
    -- print("     -----")
    
    -- Heal phase
    -- -------------------------
    if health < 0.75 then
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------
    
    -- Verdict du templier
    local spell = Helper.spell.verdict
    if Helper:usable_ranged_spell(spell) == true and power >= 5 then
        return spell
    end
    
    --
    local spell = Helper.spell.cendres
    if Helper:usable_spell(spell) == true and power >= 0 then
        return spell
    end
    
    --
    local spell = Helper.spell.cendres
    if Helper:usable_spell(spell) == true and power >= 0 then
        return spell
    end
    
    --
    local spell = Helper.spell.justice
    if Helper:usable_ranged_spell(spell) == true and power <= 3 then
        return spell
    end
    
    --
    local spell = Helper.spell.cendres
    if Helper:usable_ranged_spell(spell) == true and power == 1 then
        return spell
    end
    
    --
    local spell = Helper.spell.jugement
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    --
    local spell = Helper.spell.courroux
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    --
    local spell = Helper.spell.consecration
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    --
    local spell = Helper.spell.croise
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    --
    local spell = Helper.spell.verdict
    if Helper:usable_ranged_spell(spell) == true and power >= 3 and power <= 4 then
        return spell
    end
    
    return nil
end

local function retribution_select_cut()
    local cut = Helper.spell.reprimandes
    local casting = Helper.target.casting
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

local function retribution_select_focus_cut()
    local cut = Helper.spell.reprimandes
    local casting = Helper.focus.casting
    if casting ~= false and Helper:usable_ranged_focus_spell(cut) == true then
        return casting
    else
        return nil
    end
end

function Helper:retribution_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.mana = 0
    Helper.player.power = 0
    
    -- target info
    Helper.target = {}
    
    -- focus info
    Helper.focus = {}
    Helper.focus.casting = false
    
    -- spells
    Helper.spell = {}
    
    Helper.spell.reprimandes = "Réprimandes"
    
    Helper.spell.consecration = "Consécration"
    Helper.spell.croise = "Frappe du croisé"
    Helper.spell.jugement = "Jugement"
    Helper.spell.justice = "Lame de justice"
    Helper.spell.courroux = "Marteau de courroux"
    Helper.spell.tempete = "Tempête divine"
    Helper.spell.cendres = "Traînée de cendres"
    Helper.spell.verdict = "Verdict du templier"
    
    -- functions
    Helper.player_info = retribution_player_info
    Helper.target_info = retribution_target_info
    Helper.select_spell = retribution_select_spell
    Helper.select_cut = retribution_select_cut
    Helper.select_focus_cut = retribution_select_focus_cut
end
