Helper = {}

-- frame
Helper.frame = nil
Helper.width = 40
Helper.height = 40

Helper.alpha = 0.75

Helper.cd_frame = nil
Helper.cd_width = 100
Helper.cd_height = Helper.height

-- player
Helper.player = {}

-- target
Helper.target = {}

-- focus
Helper.focus = {}

-- spell
Helper.spell = {}

-- function
Helper.player_info = {}
Helper.target_info = {}
Helper.select_spell = {}

-- addon
Helper.next = 0
Helper.delay = 0.01
