## Interface: 80000
## Title: Helper
## Notes: ...
## Author: me
## Version: 0.1
## SavedVariables: HelperSettings

Variables.lua

Common.lua
Empty.lua

# DH
Vengeance.lua
Havoc.lua

# Monk
Brewmaster.lua 
Windwalker.lua 
Misweaver.lua

# DK
Blood.lua
Frost.lua
Unholy.lua

# Paladin
Protection.lua
Retribution.lua

# Druid
Balance.lua
Feral.lua
Guardian.lua
Restoration.lua

# Priest
Shadow.lua

# Shaman
Enhancement.lua

# Hunter
BeastMastery.lua
Survival.lua

# Warrior
Fury.lua


Helper.lua
