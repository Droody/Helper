local function havoc_player_info()
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    -- Pain
    Helper.player.pain = UnitPower("player")
    -- Metamorphose
    local name, _, _ = Helper:UnitBuff("player", "Métamorphose")
    if name == nil then
        Helper.player.metamorphose = false
    else
        Helper.player.metamorphose = true
    end
end

local function havoc_target_info()
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
end

local function havoc_select_spell()
    local health = Helper.player.health
    local pain = Helper.player.pain
    local metamorphose = Helper.player.metamorphose
    
    -- print("     -----")
    -- print("health   : " .. tostring(health))
    -- print("pain     : " .. tostring(pain))
    -- print("fragments: " .. tostring(fragments))
    -- print("fragilite: " .. tostring(fragilite))
    -- print("     -----")
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.75 then
        -- 1/ Voile
        local spell = Helper.spell.voile
        if Helper:usable_spell(spell) == true then
            return spell
        end
        spell = Helper.spell.tenebres
        if Helper:usable_spell(spell) == true then
            return spell
        end
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------

    local spell = Helper.spell.danse
    if Helper:usable_spell(spell) == true and metamorphose then
        return spell
    end

    local spell = Helper.spell.frappe
    if Helper:usable_ranged_spell(spell) == true and metamorphose then
        return spell
    end

    local spell = Helper.spell.rayon
    if Helper:usable_spell(spell) == true and not metamorphose then
        return spell
    end
    
    local spell = Helper.spell.immolation
    if Helper:usable_spell(spell) == true and pain < 70 then
        return spell
    end
    
    local spell = Helper.spell.danse
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.frappe
    if Helper:usable_ranged_spell(spell) == true and pain > 50 then
        return spell
    end
    
    local spell = Helper.spell.morsure
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    --[[
    local spell = Helper.spell.ruee
    if Helper:usable_spell(spell) == true and GetSpellCharges(spell) == 2 then
        return spell
    end
    local spell = Helper.spell.metamorphose
    if Helper:usable_spell(spell) == true and not metamorphose then
        return spell
    end
	--]] 
    
    local spell = Helper.spell.lancer
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    return nil
end

local function havoc_select_cut()
    local cut = Helper.spell.ebranlement
    local casting = Helper.target.casting
    
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

local function havoc_select_focus_cut()
    local spell = Helper.spell.metamorphose
    if Helper:usable_spell(spell) == true then
        local _, _, icon = GetSpellInfo(spell)
        return icon
    end
    return nil
end



function Helper:havoc_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.pain = 0
    Helper.player.metamorphose = false
    
    -- target info
    Helper.target = {}
    Helper.target.casting = false
    
    -- spells
    Helper.spell = {}
    -- Dps
    Helper.spell.immolation = "Aura d’immolation"
    Helper.spell.danse = "Danse des lames"
    Helper.spell.frappe = "Frappe du chaos"
    Helper.spell.lancer = "Lancer de glaive"
    Helper.spell.metamorphose = "Métamorphose"
    Helper.spell.morsure = "Morsure du démon"
    Helper.spell.nova = "Nova du chaos"
    Helper.spell.rayon = "Rayon accablant"
    -- Move
    Helper.spell.retraite = "Retraite vengeresse"
    Helper.spell.ruee = "Ruée fulgurante"
    -- Def
    Helper.spell.tenebres = "Ténèbres"
    Helper.spell.voile = "Voile corrompu"
    -- Talents
    Helper.spell.nemesis = "Némésis"
    -- Cut
    Helper.spell.ebranlement = "Ébranlement"
    
    -- functions
    Helper.player_info = havoc_player_info
    Helper.target_info = havoc_target_info
    Helper.select_spell = havoc_select_spell
    Helper.select_cut = havoc_select_cut
    Helper.select_focus_cut = havoc_select_focus_cut
end
