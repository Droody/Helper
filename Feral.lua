local function feral_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- energy
    Helper.player.energy = UnitPower("player")
    
    -- Combo
    Helper.player.combo = UnitPower("player", Enum.PowerType.ComboPoints)
    
    -- Rugissement sauvage
    local name, _, exp = Helper:UnitBuff("player", "Rugissement sauvage")
    if name == nil then
        Helper.player.rugissement = 0
    else
        Helper.player.rugissement = exp
    end
    
    -- Fureur du tigre
    local name, _, _ = Helper:UnitBuff("player", "Fureur du tigre")
    if name == nil then
        Helper.player.fureur = false
    else
        Helper.player.fureur = true
    end
    
    -- Idées claires
    local name, _, _ = Helper:UnitBuff("player", "Idées claires")
    if name == nil then
        Helper.player.idees = false
    else
        Helper.player.idees = true
    end
    
    -- Rapidité du prédateur
    local name, _, exp = Helper:UnitBuff("player", "Rapidité du prédateur")
    if name == nil then
        Helper.player.rapidite = 0
    else
        Helper.player.rapidite = exp
    end
    
    -- Rapidité du prédateur
    local name, count, _ = Helper:UnitBuff("player", "Griffes de sang")
    if name == nil then
        Helper.player.griffes = 0
    else
        Helper.player.griffes = count
    end
    
    -- Prédateur dominant
    local name, _, _ = Helper:UnitBuff("player", "Prédateur dominant")
    if name == nil then
        Helper.player.predateur = false
    else
        Helper.player.predateur = true
    end
end

local function feral_target_info()
    
    -- Health
    Helper.target.health = UnitHealth("target") / UnitHealthMax("target")
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
    
    -- Griffure
    local name, _, exp = Helper:UnitDebuff("target", "Griffure")
    if name == nil then
        Helper.target.griffure = 0
    else
        Helper.target.griffure = exp
    end
    
    -- Déchirure
    local name, _, exp = Helper:UnitDebuff("target", "Déchirure")
    if name == nil then
        Helper.target.dechirure = 0
    else
        Helper.target.dechirure = exp
    end
    
    -- Rosser
    local name, _, exp = Helper:UnitDebuff("target", "Rosser")
    if name == nil then
        Helper.target.rosser = 0
    else
        Helper.target.rosser = exp
    end

    -- Eclat lunaire
    local name, _, exp = Helper:UnitDebuff("target", "Éclat lunaire")
    if name == nil then
        Helper.target.eclat = 0
    else
        Helper.target.eclat = exp
    end
end

local function feral_select_spell()
    local health = Helper.player.health
    local energy = Helper.player.energy
    local combo = Helper.player.combo
    local idees = Helper.player.idees
    
    local rugissement = Helper.player.rugissement
    local fureur = Helper.player.fureur
    local rapidite = Helper.player.rapidite
    local griffes = Helper.player.griffes
    
    local target = Helper.target.health
    local griffure = Helper.target.griffure
    local dechirure = Helper.target.dechirure
    local rosser = Helper.target.rosser
    
    -- print("     -----")
    -- print("health   : " .. tostring(health))
    -- print("rosser: " .. tostring(rosser))
    -- print("     -----")
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.75 then
        --[[
local spell = Helper.spell.extraction
if Helper:usable_spell(spell) == true then 
return spell
end
 
local spell = Helper.spell.boisson
if Helper:usable_spell(spell) == true and health < 0.50 then 
return spell
end
 
local spell = Helper.spell.meditation
if Helper:usable_spell(spell) == true and health < 0.25 then 
return spell
end
--]]
        
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------
    
    local spell = Helper.spell.retablissment
    if Helper:usable_spell(spell) == true and Helper.talents.griffes == true and rapidite > 0 then
        return spell
    end

--[[    
    local spell = Helper.spell.retablissment
    if Helper:usable_spell(spell) == true and Helper.talents.griffes == true and rapidite > 0 and rapidite < 2 then
        return spell
    end
    
    local spell = Helper.spell.retablissment
    if Helper:usable_spell(spell) == true and Helper.talents.griffes == true and combo >= 4 and rapidite > 0 then
        return spell
    end
--]]    
    
    local spell = Helper.spell.fureur
    if Helper:usable_spell(spell) == true and energy < 30 then
        return spell
    end
--[[    
    local spell = Helper.spell.berserk
    if Helper:usable_spell(spell) == true then
        return spell
    end
--]]    
    local spell = Helper.spell.dechirure
    if Helper:usable_ranged_spell(spell) == true and dechirure <= 5 and combo >= 5 then
        return spell
    end
    
    local spell = Helper.spell.griffure
    if Helper:usable_ranged_spell(spell) == true and griffure <= 2 then
        return spell
    end
    
    local spell = Helper.spell.rosser
    if Helper:usable_spell(spell) == true and rosser <= 2 then
        return spell
    end

    local spell = Helper.spell.morsure
    -- if Helper:usable_ranged_spell(spell) == true and target < 0.25 and (dechirure <= 2 or (combo >= 5 and dechirure <= 10)) then
    if Helper:usable_ranged_spell(spell) == true and combo >= 5 and dechirure >= 5 then
        return spell
    end

    local spell = Helper.spell.balayage
    if Helper:usable_spell(spell) == true and Helper.talents.entaille == true and combo < 5 and energy >= 40 then
        return spell
    end

    local spell = Helper.spell.lambeau
    if Helper:usable_ranged_spell(spell) == true and combo < 5 and energy >= 40 then
        return spell
    end
    
--[[
    local spell = Helper.spell.rugissement
    if Helper:usable_spell(spell) == true and (rugissement <= 5 or (combo >= 5 and rugissement <= 10)) then
        return spell
    end
    
    local spell = Helper.spell.eclat
    if Helper:usable_spell(spell) == true and Helper.target.eclat < 1 and UnitExists("target") and UnitIsVisible("target") and not UnitIsFriend("player", "target") and Helper.talents.lunaire == true then
        return spell
    end
--]]    
    
    return nil
end

local function feral_select_cut()
    local cut = Helper.spell.coup
    local casting = Helper.target.casting
    
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

local function feral_select_focus_cut()
    local spell = Helper.spell.berserk
    if Helper:usable_spell(spell) == true then
        local _, _, icon = GetSpellInfo(spell)
        return icon
    end
    return nil    
    --[[
    local cut = Helper.spell.coup
    local casting = Helper.focus.casting
    
    if casting ~= false and Helper:usable_ranged_focus_spell(cut) == true then
        return casting
    else
        return nil
    end
    ]]--
end

function Helper:feral_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.energy = 0
    Helper.player.combo = 0
    Helper.player.idees = false
    Helper.player.rugissement = 0
    Helper.player.fureur = 0
    Helper.player.rapidite = 0
    Helper.player.griffes = 0
    Helper.player.predateur = false
    
    -- target info
    Helper.target = {}
    Helper.target.health = 1.0
    Helper.target.casting = false
    Helper.target.griffure = 0
    Helper.target.dechirure = 0
    Helper.target.rosser = 0
    Helper.target.eclat = 0
    
    -- focus info
    Helper.focus = {}
    Helper.focus.casting = false
    
    -- spells
    Helper.spell = {}
    Helper.spell.balayage = "Balayage"
    Helper.spell.dechirure = "Déchirure"
    Helper.spell.eclat = "Éclat lunaire"
    Helper.spell.fureur = "Fureur du tigre"
    Helper.spell.griffure = "Griffure"
    Helper.spell.lambeau = "Lambeau"
    Helper.spell.morsure = "Morsure féroce"
    Helper.spell.rosser = "Rosser" --(Combat farouche, Gardien)"
    Helper.spell.retablissment = "Rétablissement"
    Helper.spell.berserk = "Berserk"
    
    Helper.spell.rugissement = "Rugissement sauvage"
    
    -- def
    Helper.spell.infu_pur = ""
    Helper.spell.extraction = ""
    
    -- cut
    Helper.spell.coup = "Coup de crâne"
    
    -- Talents
    Helper.talents = {}
    Helper.talents.predateur = false
    Helper.talents.lunaire = false
    Helper.talents.entaille = false
    Helper.talents.rugissement = false
    Helper.talents.griffes = false
    
    local _, _, _, selected, _ = GetTalentInfo(1, 2, 1)
    if selected == true then
        print("> Prédateur")
        Helper.talents.predateur = true
    end
    local _, _, _, selected, _ = GetTalentInfo(1, 3, 1)
    if selected == true then
        print("> Inspiration lunaire")
        Helper.talents.lunaire = true
    end
    local _, _, _, selected, _ = GetTalentInfo(6, 2, 1)
    if selected == true then
        print("> Entaille brutale")
        Helper.talents.entaille = true
    end
    local _, _, _, selected, _ = GetTalentInfo(6, 3, 1)
    if selected == true then
        print("> Rugissement sauvage")
        Helper.talents.rugissement = true
    end
    local _, _, _, selected, _ = GetTalentInfo(7, 2, 1)
    if selected == true then
        print("> Griffes de sang")
        Helper.talents.griffes = true
    end
    
    -- functions
    Helper.player_info = feral_player_info
    Helper.target_info = feral_target_info
    Helper.select_spell = feral_select_spell
    Helper.select_cut = feral_select_cut
    Helper.select_focus_cut = feral_select_focus_cut
end
