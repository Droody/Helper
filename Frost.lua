local function frost_player_info()

	-- Health
	Helper.player.health = UnitHealth("player") / UnitHealthMax("player")

	-- Runic Power
	Helper.player.power   = UnitPower("player")

	-- Runes
	Helper.player.runes = 0
	local max = UnitPowerMax("player", SPELL_POWER_RUNES)
	for runeIndex = 1, max do
		local start, duration, runeReady = GetRuneCooldown(runeIndex)
		if runeReady then
			Helper.player.runes = Helper.player.runes + 1
		end
	end

	-- Frimas
	local name, rank, icon, count, debuffType, duration, expirationTime, unitCaster, isStealable, shouldConsolidate, spellId = UnitBuff("player", "Frimas")
	if name == nil then
		Helper.player.frimas = false
	else
		local exp = expirationTime - GetTime()
		if exp < 1 then
			Helper.player.frimas = false
		else
			Helper.player.frimas = true
		end
	end

	-- Pilier
	local name, rank, icon, count, debuffType, duration, expirationTime, unitCaster, isStealable, shouldConsolidate, spellId = UnitBuff("player", "Pilier de givre")
	if name == nil then
		Helper.player.pilier = false
	else
		local exp = expirationTime - GetTime()
		if exp < 1 then
			Helper.player.pilier = false
		else
			Helper.player.pilier = true
		end
	end
end

local function frost_target_info()

	-- Peste de sang
	local name, rank, icon, count, debuffType, duration, expirationTime, unitCaster, isStealable, shouldConsolidate, spellId = UnitDebuff("target", "Peste de sang", nil , "player")
	if name == nil then
		Helper.target.peste = false
	else
		local exp = expirationTime - GetTime()
		if exp < 2 then Helper.target.peste = false
		else Helper.target.peste = true
		end
	end

	-- Casting
	local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo("target")
	if spell ~= nil and interrupt == false then
		Helper.target.casting = icon
	else
		Helper.target.casting = false
	end
end

local function frost_select_spell()
	local health = Helper.player.health
	local power  = Helper.player.power
	local runes  = Helper.player.runes
	local frimas = Helper.player.frimas
	local pilier = Helper.player.pilier

	-- print("     -----")
	-- print("health   : " .. tostring(health))
	-- print("power    : " .. tostring(power))
	-- print("runes    : " .. tostring(runes))
	-- print("bouclier   : " .. tostring(bouclier))
	-- print("peste    : " .. tostring(peste))
	-- print("     -----")

	-- -------------------------
	-- Heal phase
	-- -------------------------
	if health < 0.75 then

		local spell = Helper.spell.mort
		if Helper:usable_ranged_spell(spell) == true then 
			return spell
		end

		if health < 0.5 then
			local spell = Helper.spell.robustesse
			if Helper:usable_spell(spell) == true then 
				return spell
			end
		end
	end

	-- -------------------------
	-- DPS phase
	-- -------------------------

	local spell = Helper.spell.obliteration
	if Helper:usable_spell(spell) == true and pilier == true then 
		return spell
	end

	local spell = Helper.spell.arme
	if Helper:usable_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.hiver
	if Helper:usable_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.rafale
	if Helper:usable_spell(spell) == true and frimas == true then 
		return spell
	end

	local spell = Helper.spell.aneantissement
	if Helper:usable_ranged_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.frappe
	if Helper:usable_ranged_spell(spell) == true then 
		return spell
	end

	local spell = Helper.spell.renforcer
	if Helper:usable_spell(spell) == true then 
		return spell
	end

	return nil
end

local function frost_select_cut()
	local cut = Helper.spell.gel
	local casting = Helper.target.casting
	if casting ~= false and Helper:usable_ranged_spell(cut) == true then
		return casting
	else
		return nil
	end
end

function Helper:frost_init()
	-- player info
	Helper.player = {}
	Helper.player.health = 1.0
	Helper.player.power  = 0
	Helper.player.runes  = 0
	Helper.player.frimas = false
	Helper.player.pilier = false

	-- target info
	Helper.target = {}
	-- Helper.target.peste  = false
	Helper.target.casting = false

	-- spells
	Helper.spell = {}

	Helper.spell.aneantissement = "Anéantissement"
	Helper.spell.frappe         = "Frappe de givre"
	Helper.spell.hiver          = "Hiver impitoyable"
	Helper.spell.obliteration   = "Oblitération"
	Helper.spell.pilier         = "Pilier de givre"
	Helper.spell.rafale         = "Rafale hurlante"
	Helper.spell.renforcer      = "Renforcer l'arme runique"

	Helper.spell.arme           = "Fureur de Sindragosa(Arme prodigieuse)"

	Helper.spell.mort           = "Frappe de mort"
	Helper.spell.robustesse     = "Robustesse glaciale"

	Helper.spell.gel            = "Gel de l'esprit"
	
	-- functions
	Helper.player_info  = frost_player_info
	Helper.target_info  = frost_target_info
	Helper.select_spell = frost_select_spell
	Helper.select_cut   = frost_select_cut
end
