local function windwalker_player_info()
	-- Health
	Helper.player.health = UnitHealth("player") / UnitHealthMax("player")

	-- energy
	Helper.player.energy = UnitPower("player")

	-- Chi
	Helper.player.chi = UnitPower("player", Enum.PowerType.Chi)

	--[[
	-- TTF
	local name, rank, icon, count, debuffType, duration, expirationTime, unitCaster, isStealable, shouldConsolidate, spellId = UnitBuff("player", "Tempête, Terre et Feu")
	if name == nil then
		Helper.player.ttf = false
	else
		Helper.player.ttf = true
	end
	--]]
end

local function windwalker_target_info()
	-- Casting
	local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId =
		UnitCastingInfo("target")
	if name ~= nil and notInterruptible == false then
		Helper.target.casting = texture
	else
		Helper.target.casting = false
	end

	-- Focus Casting
	local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo("focus")
	if spell ~= nil then
		Helper.focus.casting = icon
	else
		Helper.focus.casting = false
	end
end

local function windwalker_select_spell()
	local health = Helper.player.health
	local energy = Helper.player.energy
	local chi = Helper.player.chi
	-- local ttf    = Helper.player.ttf

	-- print("     -----")
	-- print("health   : " .. tostring(health))
	-- print("pain     : " .. tostring(pain))
	-- print("fragments: " .. tostring(fragments))
	-- print("fragilite: " .. tostring(fragilite))
	-- print("chi   : " .. tostring(chi))
	-- print("energy: " .. tostring(energy))
	-- print("     -----")

	-- -------------------------
	-- Heal phase
	-- -------------------------
	if health < 0.75 then
	--[[ 
		local spell = Helper.spell.extraction
		if Helper:usable_spell(spell) == true then 
			return spell
		end

		local spell = Helper.spell.boisson
		if Helper:usable_spell(spell) == true and health < 0.50 then 
			return spell
		end

		local spell = Helper.spell.meditation
		if Helper:usable_spell(spell) == true and health < 0.25 then 
			return spell
		end
		--]]
	end

	-- -------------------------
	-- DPS phase
	-- -------------------------

	local spell = Helper.spell.mortel
	if Helper:usable_ranged_spell(spell) == true then
		return spell
	end

	local spell = Helper.spell.blanc
	if Helper:usable_ranged_spell(spell) == true and chi < 3 and energy > 70 then
		return spell
	end

	local spell = Helper.spell.tigre
	if Helper:usable_ranged_spell(spell) == true and chi < 4 and energy > 70 then
		return spell
	end

	local spell = Helper.spell.dragon
	if Helper:usable_spell(spell) == true then
		return spell
	end

	local spell = Helper.spell.fureur
	if Helper:usable_spell(spell) == true then
		return spell
	end

	local spell = Helper.spell.soleil
	if Helper:usable_ranged_spell(spell) == true then
		return spell
	end

	local spell = Helper.spell.blanc
	if Helper:usable_ranged_spell(spell) == true then
		return spell
	end

	local spell = Helper.spell.voile
	if Helper:usable_ranged_spell(spell) == true then
		return spell
	end

	--[[
	local spell = Helper.spell.explosion
	if Helper:usable_spell(spell) == true then 
		return spell
	end
	--]]
	local spell = Helper.spell.tigre
	if Helper:usable_ranged_spell(spell) == true then
		return spell
	end

	return nil
end

local function windwalker_select_cut()
	local cut = Helper.spell.pique
	local casting = Helper.target.casting
	if casting ~= false and Helper:usable_ranged_spell(cut) == true then
		return casting
	else
		return nil
	end
end

local function windwalker_select_focus_cut()
	local cut = Helper.spell.pique
	local casting = Helper.focus.casting
	if casting ~= false and Helper:usable_ranged_focus_spell(cut) == true then
		return casting
	else
		return nil
	end
end

function Helper:windwalker_init()
	-- player info
	Helper.player = {}
	Helper.player.health = 1.0
	Helper.player.energy = 0
	Helper.player.chi = 0
	-- Helper.player.ttf    = false

	-- target info
	Helper.target = {}
	Helper.target.casting = false

	-- focus info
	Helper.focus = {}
	Helper.focus.casting = false

	-- spells
	Helper.spell = {}
	Helper.spell.soleil = "Coup de pied du soleil levant"
	-- Helper.spell.grue   = "Coup tournoyant de la grue"
	Helper.spell.voile = "Frappe du voile noir"
	Helper.spell.tigre = "Paume du tigre"
	Helper.spell.fureur = "Poings de fureur"
	Helper.spell.ttf = "Tempête, Terre et Feu"
	Helper.spell.mortel = "Toucher mortel"

	-- talents
	Helper.spell.explosion = "Explosion de chi"
	Helper.spell.blanc = "Poing du Tigre blanc"
	Helper.spell.dragon = "Coup de poing du dragon tourbillonnant"

	-- Def
	Helper.spell.infu_pur = "Toucher du karma"
	Helper.spell.extraction = "Élixir de soins"

	-- cut
	Helper.spell.pique = "Pique de main"

	-- functions
	Helper.player_info = windwalker_player_info
	Helper.target_info = windwalker_target_info
	Helper.select_spell = windwalker_select_spell
	Helper.select_cut = windwalker_select_cut
	Helper.select_focus_cut = windwalker_select_focus_cut
end
