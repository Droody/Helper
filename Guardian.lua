local function guardian_player_info()
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    -- Rage
    Helper.player.rage = UnitPower("player")
    -- Galactique
    local name, _, _ = Helper:UnitBuff("player", "Gardien galactique")
    if name ~= nil then
        Helper.player.galactique = true
    else
        Helper.player.galactique = false
    end
    -- Ferpoil
    local name, _, exp = Helper:UnitBuff("player", "Ferpoil")
    if name == nil then
        Helper.player.ferpoil = 0
    else
        Helper.player.ferpoil = exp
    end
end

local function guardian_target_info()
    -- Eclat lunaire
    local name, _, _ = Helper:UnitDebuff("target", "Éclat lunaire")
    if name == nil then
        Helper.target.eclat = false
    else
        Helper.target.eclat = true
    end
    -- Rosser
    local name, count, _ = Helper:UnitDebuff("target", "Rosser")
    if name == nil then
        Helper.target.rosser = 0
    else
        Helper.target.rosser = count
    end
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
end

local function guardian_select_spell()
    --[[    for i = 1, 40 do
        local name, icon, count, debuffType, duration, expirationTime, _ = UnitDebuff("target", i, nil, "PLAYER")
        if name then
            print(("%d=%s, %s"):format(i, name, tostring(icon)))
        end
    end
--]] 
    local health = Helper.player.health
    local rage = Helper.player.rage
    local ferpoil = Helper.player.ferpoil
    local galactique = Helper.player.galactique
    local eclat = Helper.target.eclat
    local rosser = Helper.target.rosser
    
    -- print("     -----")
    -- print("health    : " .. tostring(health))
    -- print("rage      : " .. tostring(rage))
    -- print("galactique: " .. tostring(galactique))
    -- print("eclat     : " .. tostring(eclat))
    -- print("rosser    : " .. tostring(rosser))
    -- print("     -----")
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.75 then
        -- 0/ Ferpoil
        local spell = Helper.spell.ferpoil
        if Helper:usable_spell(spell) == true and ferpoil <= 0 then
            return spell
        end
        -- 1/ Regeneration
        local spell = Helper.spell.regeneration
        if Helper:usable_spell(spell) == true then
            return spell
        end
        -- 2/ Ferpoil
        local spell = Helper.spell.ferpoil
        if Helper:usable_spell(spell) == true then
            return spell
        end
        -- 4/ Ecorse
        local spell = Helper.spell.ecorse
        if Helper:usable_spell(spell) == true then
            return spell
        end
        -- 5/ Instincts
        local spell = Helper.spell.instincts
        if Helper:usable_spell(spell) == true then
            return spell
        end
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------
    
    -- 0/ Avoid Rage Cap
    if rage >= 80 then
        local spell = Helper.spell.ferpoil
        if Helper:usable_spell(spell) == true and ferpoil <= 0 then
            return spell
        end
        local spell = Helper.spell.mutiler
        -- print(spell .. ": " .. tostring(Helper:usable_ranged_spell(spell)))
        if Helper:usable_ranged_spell(spell) == true and health >= 0.9 then
            return spell
        end
    end
    
    -- 1/ Eclat lunaire
    local spell = Helper.spell.eclat
    -- print(spell .. ": " .. tostring(Helper:usable_ranged_spell(spell)))
    if Helper:usable_ranged_spell(spell) == true and eclat == false then
        return spell
    end
    -- 2/ Rosser
    local spell = Helper.spell.rosser
    -- print(spell .. ": " .. tostring(Helper:usable_ranged_spell(spell)))
    if Helper:usable_spell(spell) == true and rosser < 3 then
        return spell
    end
--[[
    -- 3/ Pulverisation
    local spell = Helper.spell.pulverisation
    -- print(spell .. ": " .. tostring(Helper:usable_ranged_spell(spell)))
    if Helper:usable_ranged_spell(spell) == true and rosser >= 3 then
        return spell
    end
--]] 
   -- 4/ Mutilation
    local spell = Helper.spell.mutilation
    -- print(spell .. ": " .. tostring(Helper:usable_ranged_spell(spell)))
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    -- 5/ Rosser
    local spell = Helper.spell.rosser
    -- print(spell .. ": " .. tostring(Helper:usable_ranged_spell(spell)))
    if Helper:usable_spell(spell) == true then
        return spell
    end
    -- 6/ Eclat lunaire si galactique
    local spell = Helper.spell.eclat
    -- print(spell .. ": " .. tostring(Helper:usable_ranged_spell(spell)))
    if Helper:usable_ranged_spell(spell) == true and galactique == true then
        return spell
    end
    -- 7/ Balayage
    local spell = Helper.spell.balayage
    -- print(spell .. ": " .. tostring(Helper:usable_ranged_spell(spell)))
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    return nil
end

local function guardian_select_cut()
    local cut = Helper.spell.coup
    local casting = Helper.target.casting
    
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

local function guardian_select_focus_cut()
    local cut = Helper.spell.coup
    local casting = Helper.focus.casting
    
    if casting ~= false and Helper:usable_ranged_focus_spell(cut) == true then
        return casting
    else
        return nil
    end
end

function Helper:guardian_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.rage = 0
    --
    Helper.player.galactique = false
    Helper.player.ferpoil = 0
    
    -- target info
    Helper.target = {}
    Helper.target.eclat = false
    Helper.target.rosser = 0
    Helper.target.casting = false
    
    -- focus info
    Helper.focus = {}
    Helper.focus.casting = false
    
    -- spells
    Helper.spell = {}
    Helper.spell.balayage = "Balayage"
    Helper.spell.coup = "Coup de crâne"
    Helper.spell.eclat = "Éclat lunaire(Lunaire)"
    Helper.spell.ecorse = "Ecorce"
    Helper.spell.ferpoil = "Ferpoil"
    Helper.spell.instincts = "Instincts de survie"
    Helper.spell.marque = "Marque d’Ursol"
    Helper.spell.mutilation = "Mutilation"
    Helper.spell.mutiler = "Mutiler"
    Helper.spell.pulverisation = "Pulvérisation"
    Helper.spell.regeneration = "Régénération frénétique"
    Helper.spell.rosser = "Rosser"
    
    -- functions
    Helper.player_info = guardian_player_info
    Helper.target_info = guardian_target_info
    Helper.select_spell = guardian_select_spell
    Helper.select_cut = guardian_select_cut
end
