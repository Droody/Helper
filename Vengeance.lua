local function vengeance_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- Pain
    Helper.player.pain = UnitPower("player")
    
    -- Fragments
    local name, count, _ = Helper:UnitBuff("player", "Fragments d’âme")
    if name == nil or count == 0 then
        Helper.player.fragments = 0
    else
        Helper.player.fragments = count
    end
    
    -- Pointes
    local name, _, _ = Helper:UnitBuff("player", "Pointes démoniaques")
    if name == nil then
        Helper.player.pointes = false
    else
        Helper.player.pointes = true
    end
end

local function vengeance_target_info()
    
    -- Fragilite
    local name, _, exp = Helper:UnitDebuff("target", "Fragilité", nil, "player")
    if name == nil then
        Helper.target.fragilite = false
    else
        if exp < 5 then Helper.target.fragilite = false
        else Helper.target.fragilite = true
        end
    end
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
    
    -- Marque
    local name, _, _ = Helper:UnitDebuff("target", "Marque enflammée")
    if name == nil then
        Helper.target.marque = false
    else
        Helper.target.marque = true
    end
    
    -- Sigil
    local name, _, _ = Helper:UnitDebuff("target", "Sigil de feu")
    if name == nil then
        Helper.target.sigil = false
    else
        Helper.target.sigil = true
    end
end

local function vengeance_select_spell()
    local health = Helper.player.health
    local pain = Helper.player.pain
    local fragments = Helper.player.fragments
    local pointes = Helper.player.pointes
    local fragilite = Helper.target.fragilite
    local marque = Helper.target.marque
    local sigil = Helper.target.sigil
    
    -- print("     -----")
    -- print("health   : " .. tostring(health))
    -- print("pain     : " .. tostring(pain))
    -- print("fragments: " .. tostring(fragments))
    -- print("fragilite: " .. tostring(fragilite))
    -- print("     -----")
    
    local spell = Helper.spell.infusion
    if Helper:usable_spell(spell) == true and GetSpellCharges(Helper.spell.pointes) <= 0 then
        return spell
    end
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.75 then
        -- 0/ Fragilite
        local spell = Helper.spell.bombe
        if Helper:usable_spell(spell) == true and fragilite == false and fragments > 0 then
            return spell
        end
        -- 1/ Pointes si 2 charges
        local spell = Helper.spell.pointes
        if Helper:usable_spell(spell) == true and GetSpellCharges(spell) == 2 and pointes == false then
            return spell
        end
        -- 2/ Marque
        local spell = Helper.spell.marque
        if Helper:usable_spell(spell) == true then
            return spell
        end
        -- 3/ Division
        local spell = Helper.spell.division
        if Helper:usable_spell(spell) == true then
            return spell
        end
        -- 4/ Pointes
        local spell = Helper.spell.pointes
        if Helper:usable_spell(spell) == true and pointes == false then
            return spell
        end
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------
    
    -- 1/ Fragilité
    local spell = Helper.spell.bombe
    if Helper:usable_spell(spell) == true and fragments >= 4 then
        return spell
    end
    
    -- 2/ Fracture if pain <= 80 and fragments <= 3
    local spell = Helper.spell.entaille
    if Helper:usable_ranged_spell(spell) == true and pain <= 80 and fragments <= 3 then
        return spell
    end
    
    -- 3/ Immolation
    local spell = Helper.spell.immolation
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    local spell = Helper.spell.bombe
    if Helper:usable_spell(spell) == true and fragilite == false then
        return spell
    end

    -- 4/ Division
    local spell = Helper.spell.division
    if Helper:usable_spell(spell) == true and pain > 70 and fragilite == true then
        return spell
    end
    
    local spell = Helper.spell.frappe
    if Helper:usable_spell(spell) == true and GetSpellCharges(spell) >= 2 and sigil == false then
        return spell
    end
    
    -- 5/ Sigil de feu is sigil == false
    local spell = Helper.spell.sigil
    if Helper:usable_spell(spell) == true and sigil == false then
        return spell
    end
    
    -- 6/ Lancer
    local spell = Helper.spell.lancer
    if Helper:usable_spell(spell) == true then
        return spell
    end
    
    return nil
end

local function vengeance_select_cut()
    local cut = Helper.spell.manavore
    local casting = Helper.target.casting
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

local function vengeance_select_focus_cut()
    local cut = Helper.spell.manavore
    local casting = Helper.focus.casting
    
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

function Helper:vengeance_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.pain = 0
    Helper.player.fragments = 0
    Helper.player.pointes = false
    
    -- target info
    Helper.target = {}
    Helper.target.fragilite = false
    Helper.target.casting = false
    Helper.target.marque = false
    Helper.target.sigil = false
    
    -- focus info
    Helper.focus = {}
    Helper.focus.casting = false
    
    -- spells
    Helper.spell = {}
    Helper.spell.immolation = "Aura d’immolation"
    Helper.spell.barriere = "Barrière d’âme"
    Helper.spell.bombe = "Bombe spirituelle"
    Helper.spell.dechirement = "Déchirement d’âme(Arme prodigieuse)"
    Helper.spell.devastation = "Dévastation gangrenée"
    Helper.spell.division = "Division de l’âme"
    Helper.spell.entaille = "Entaille"
    Helper.spell.frappe = "Frappe infernale"
    Helper.spell.gangrelame = "Gangrelame"
    Helper.spell.lancer = "Lancer de glaive"
    Helper.spell.manavore = "Manavore"
    Helper.spell.marque = "Marque enflammée"
    Helper.spell.pointes = "Pointes démoniaques"
    Helper.spell.sigil = "Sigil de feu"
    Helper.spell.infusion = "Infusion démoniaque"
    
    -- functions
    Helper.player_info = vengeance_player_info
    Helper.target_info = vengeance_target_info
    Helper.select_spell = vengeance_select_spell
    Helper.select_cut = vengeance_select_cut
    Helper.select_focus_cut = vengeance_select_focus_cut
end
