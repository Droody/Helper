local function enhancement_player_info()
    
    -- Health
    Helper.player.health = UnitHealth("player") / UnitHealthMax("player")
    
    -- Power
    Helper.player.power = UnitPower("player")
    
    -- Arme de givre
    local name, _, exp = Helper:UnitBuff("player", "Arme de givre")
    if name == nil then
        Helper.player.givre = 0
    else
        Helper.player.givre = exp
    end
    
    -- Langue de feu
    local name, _, exp = Helper:UnitBuff("player", "Langue de feu")
    if name == nil then
        Helper.player.feu = 0
    else
        Helper.player.feu = exp
    end
    
    -- Bouclier de foudre
    local name, _, exp = Helper:UnitBuff("player", "Bouclier de foudre")
    if name == nil then
        Helper.player.foudre = false
    else
        Helper.player.foudre = true
    end
    
    -- Bouclier de terre
    local name, _, exp = Helper:UnitBuff("player", "Bouclier de terre")
    if name == nil then
        Helper.player.terre = false
    else
        Helper.player.terre = true
    end
    
    --[[
    -- 
    local name, count, exp = Helper:UnitBuff("player", "Renforcement lunaire")
    if name == nil then
        Helper.player.lunaire = 0
    else
        Helper.player.lunaire = count
    end
    --]]
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("player")
    if name then
        Helper.player.casting = name
    else
        Helper.player.casting = nil
    end
end

local function enhancement_target_info()
    
    -- Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("target")
    if name ~= nil and notInterruptible == false then
        Helper.target.casting = texture
    else
        Helper.target.casting = false
    end
    
    -- Focus Casting
    local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("focus")
    if name ~= nil and notInterruptible == false then
        Helper.focus.casting = texture
    else
        Helper.focus.casting = false
    end
end

local function enhancement_select_spell()
    local health = Helper.player.health
    local power = Helper.player.power
    -- local oneth = Helper.player.oneth
    local givre = Helper.player.givre
    local feu = Helper.player.feu
    local foudre = Helper.player.foudre
    local terre = Helper.player.terre
    local casting = Helper.player.casting
    
    -- print("     -----")
    -- print("health    : " .. tostring(health))
    -- print("lunaire   : " .. tostring(lunaire))
    -- print("feu      : " .. tostring(feu))
    -- print("givre   : " .. tostring(givre))
    -- print("lunaire   : " .. tostring(renf_lunaire))
    -- print("oneth   : " .. tostring(oneth))
    -- print("meteores: " .. tostring(meteores))
    -- print("casting   : " .. tostring(casting))
    -- print("     -----")
    
    -- -------------------------
    -- Heal phase
    -- -------------------------
    if health < 0.25 then
        -- Ecorse
        local spell = Helper.spell.afflux
        if Helper:usable_spell(spell) == true and spell ~= casting then
            return spell
        end
    end
    
    -- -------------------------
    -- DPS phase
    -- -------------------------
    -- 1/ Not in Compobat -> courroux
    -- local spell = Helper.spell.courroux
    -- if Helper:usable_ranged_spell(spell) == true and not UnitAffectingCombat("player") then
    -- return spell
    -- end
    
    -- Bouclier de foudre
    local spell = Helper.spell.bouclier_foudre
    if Helper:usable_spell(spell) == true and foudre == false then
        return spell
    end
    
    -- Bouclier de terre
    local spell = Helper.spell.bouclier_terre
    if Helper:usable_spell(spell) == true and terre == false then
        return spell
    end
    
    -- Impulsion Farouche
    local spell = Helper.spell.impulsion
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    -- Langue de Feu
    local spell = Helper.spell.langue
    if Helper:usable_ranged_spell(spell) == true and feu <= 4.5 then
        return spell
    end
    
    -- Arme de Givre
    local spell = Helper.spell.arme
    if Helper:usable_ranged_spell(spell) == true and givre <= 4.5 then
        return spell
    end

    -- Frappe-tempête
    local spell = Helper.spell.frappe
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    -- Croque-roc
    local spell = Helper.spell.croque
    if Helper:usable_ranged_spell(spell) == true and power <= 70 then
        return spell
    end

    -- Fouet de lave
    local spell = Helper.spell.fouet
    if Helper:usable_ranged_spell(spell) == true and power >= 40 then
        return spell
    end
    
    -- Croque-roc
    local spell = Helper.spell.croque
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end

    -- Langue de Feu
    local spell = Helper.spell.langue
    if Helper:usable_ranged_spell(spell) == true then
        return spell
    end
    
    return nil
end

local function enhancement_select_cut()
    local cut = Helper.spell.cisaille
    local casting = Helper.target.casting
    
    if casting ~= false and Helper:usable_ranged_spell(cut) == true then
        return casting
    else
        return nil
    end
end

function Helper:enhancement_init()
    -- player info
    Helper.player = {}
    Helper.player.health = 1.0
    Helper.player.power = 1.0
    Helper.player.feu = 0
    Helper.player.givre = 0
    Helper.player.foudre = 0
    Helper.player.terre = 0
    Helper.player.casting = nil
    
    -- target info
    Helper.target = {}
    
    -- spells
    Helper.spell = {}
    Helper.spell.afflux = "Afflux de soins"
    Helper.spell.arme = "Arme de givre"
    Helper.spell.croque = "Croque-roc"
    Helper.spell.fouet = "Fouet de lave"
    Helper.spell.frappe = "Frappe-tempête"
    Helper.spell.langue = "Langue de feu"
    Helper.spell.impulsion = "Impulsion farouche"

    Helper.spell.cisaille = "Cisaille de vent"
    
    Helper.spell.bouclier_foudre = "Bouclier de foudre"
    Helper.spell.bouclier_terre = "Bouclier de terre"
    
    -- functions
    Helper.player_info = enhancement_player_info
    Helper.target_info = enhancement_target_info
    Helper.select_spell = enhancement_select_spell
    Helper.select_cut = enhancement_select_cut
end
